/**
 * Show trailer
 * @author Andreas Svitzer
 */
function showTrailer() {
	// Hide image
	var image = document.getElementById("image");
	image.className += " hidden";

	// Show trailer
	var trailer = document.getElementById("trailer");
	trailer.className = "trailer";
}	