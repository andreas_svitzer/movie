# Get started

	Copy .env.example => .env 
	Set env to test, develop, production 

	npm install
	npm install -g bower
	npm install --global gulp-cli
	npm start

	Go to http://localhost:3100/film/the-sound-of-music-1965

Make sure the following dependencies are installed 

	Bower install susy --save (sass grid)


# Node JS boilerplate

(https://github.com/madhums/node-express-mongoose)


# Bower

(https://bower.io/)


# GULP

(http://gulpjs.com/)

**Compile sass + minify css**
gulp sass:compress

**Minify css + js**
gulp compress

**Compile sass to css**
gulp sass

**Watch for changes in sass**
gulp sass:watch

**Minify js**
gulpcompress-js

**Minify css**
compress-css


# Mocha

	npm install -g mocha

Run mocha in root to run tests

## Jade

(http://jade-lang.com/)


# API's in use

Viaplay Content API

Trailer Addict API (waiting for API key) 


# TODO
	
1. Create a real page for home.
2. Create middlewares, cache + url validation.
3. Style error pages.
4. Responsivness for all type of devices.
5. Responsive footer.
6. Responsive header nav.
7. Change trailer picture depending on screensize, optimization.
8. Create more tests, endpoints + logic in /test folder.
9. Check that all data that we need is presented to us. If not handle it in a nice way.
10. Canonical url.
11. Fav icon.
12. Go throught meta data.


# Short info about tech choices

**node-express-mongoose** good starting point for my Node JS application. Containing everything I need an a little bit more candy for the future, but not to much over head. No need to rebuild the wheel.

**Bower** Grab nice assets from the internet in a uniformed way.

**Gulp** Manage my assets, compile sass, compress and more. Very usefull as the application grows.

**Mocha** My favorite testing package for Node JS. Easy to use.

**Jade** Came with the boilterplate. Nice way of writing my markup.


# Stuff I left out

Integration to Trailer Addict API, missing api_key and no time.

Autoplay for trailers, missing documentation and no response from owners.

Middlewares for cache and validation. No time. 

Extended testing is needed to make sure everything is okey with the application. Not time.


# Notes

MongoDB disabled untill needed.

Passport and user model not in use.

Missing api_key from Trailer Addict API. No response from owners.

Missing function for autoplay trailer when switch from image to trailer. Can't find any documentation on how this is done and if it's possible since it's an embedded flash player. If possible some setting in iframe. Waiting for response from Trailer Addict API.

I can't seem to find the category in Viaplay Content API. Fakeing it for now.

Can't get a trailer from Imdb id because of missing api_key, fakeing it for now.