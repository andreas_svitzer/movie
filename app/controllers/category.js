
/*!
 * Module dependencies.
 */
var api = require('../libraries/api');

exports.index = function (req, res) {

	// Get data
	api.getByCategoryAndId(req.params.cat, req.params.id, function(error, response, data) {

		// Render view
		if ( ! data || error || response.status == 200 ) {
	  		
			res.status(404) // HTTP status 404: NotFound
   				.send('Not found');

	  	} else {

	  		res.render('category/index', {
	    		title: data.title,
	    		description: data.description,
	    		metaImage: data.images.landscape.url,
	    		url: '',
	    		data: data
	  		});

	  	}

	  	// Return
	  	return;
	});

  	// Return
  	return;
};
