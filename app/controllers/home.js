
/*!
 * Module dependencies.
 */

exports.index = function (req, res) {

	// Render view
  	res.render('home/index', {
    	title: 'Home',
    	desciption: 'This is home',
    	metaImage: '',
    	url: ''
  	});

  	// Return
  	return;
};
