// Setup obj
var movieTrailerApi = {};
var request = require('request');

/**
 * @author Andreas Svitzer
 * Get data
 */
movieTrailerApi.getByImdbId = function(id, masterCallback) {

	/**
	 * TODO: Get from Trailer Addict API.
	 */

	// Trailer
	var trailer = {};
	trailer = getTrailer();

	// Callback
	masterCallback(trailer);

	// Return
	return;
}

/**
 * @author Andreas Svitzer
 * Get trailer
 */
var getTrailer = function() {
	return "<link rel='stylesheet' type='text/css' href='//cdn.traileraddict.com/css/rembed.css'><div class='outer-embed-ta'><iframe width='100%' src='//v.traileraddict.com/0059742' allowfullscreen='true' webkitallowfullscreen='true' autoplay='true' mozallowfullscreen='true' scrolling='no' class='embed-ta'></iframe></div>";
}

// Export
module.exports = movieTrailerApi;