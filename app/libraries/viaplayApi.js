// Setup obj
var viaplayApi = {};
var request = require('request');
var apiBaseUrl = 'https://content.viaplay.se/pc-se';
var data = {};

/** 
 * @author Andreas Svitzer
 * Get data from API
 */
viaplayApi.get = function(url, masterCallback) {

	// Set options
	var options = {
		method: 'GET',
	  	url: url,
	  	headers: {
	  		'User-Agent': 'request'
	  	}
	};

	// Callback
	var callback = function (error, response, body) {

		// Check response
	  	if (!error && response.statusCode == 200) {

	  		// Parse JSON
	  		data = JSON.parse(body);

	  		// Callback
	  		masterCallback(error, response, data);
	  	} else {

	  		// Callback
	  		masterCallback(error, response);	  		
	  	}

  		// Return
  		return;
	}

	// Do request
	request(options, callback);

	// Return
	return;
}

/**
 * @author Andreas Svitzer
 * Get data
 */
viaplayApi.getByCategoryAndId = function(category, id, masterCallback) {

	// Set url
	var url = apiBaseUrl + '/' + category + '/' + id;

	// Set options
	var options = {
		method: 'GET',
	  	url: url,
	  	headers: {
	  		'User-Agent': 'request'
	  	}
	};

	// Callback
	viaplayApi.get(url, function(error, response, data) {

		// Check response
	  	if (!error && response.statusCode == 200) {

	  		// Map data
	  		var mappedData = {};

	  		mappedData.imdb = getImdb();
	  		mappedData.title = getTitle();
	  		mappedData.description = getDescription();
	  		mappedData.year = getYear();
	  		mappedData.country = getCountry();
	  		mappedData.time = getTime();
	  		mappedData.images = getImages();
	  		mappedData.people = getPeople();
			mappedData.parentalRating = getParentalRating();	
			mappedData.flags = getFlags();	
			mappedData.category = getCategory();

	  		// Callback
	  		masterCallback(error, response, mappedData);
	  	} else {

	  		// Callback
	  		masterCallback(error, response);	  		
	  	}

  		// Return
  		return;

	});

	// Return
	return;
}

/**
 * @author Andreas Svitzer
 * Get title
 */
var getTitle = function() {
	return data.title;
};

/**
 * @author Andreas Svitzer
 * Get title
 */
var getDescription = function() {
	return data.description;
};

/**
 * @author Andreas Svitzer
 * Get Imdb data
 */
var getImdb = function() {
	return data._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.imdb;
};

/**
 * @author Andreas Svitzer
 * Get year
 */
var getYear = function() {
	return data._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.production.year;
};

/**
 * @author Andreas Svitzer
 * Get country
 */
var getCountry = function() {
	return data._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.production.country;
};

/**
 * @author Andreas Svitzer
 * Get time
 */
var getTime = function() {
	return data._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.duration.readable;
}

/**
 * @author Andreas Svitzer
 * Get images
 */
var getImages = function() {
	return data._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.images;
}

/**
 * @author Andreas Svitzer
 * Get people
 */
var getPeople = function() {
	return data._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.people;
}

/**
 * @author Andreas Svitzer
 * Get parental rating
 */
var getParentalRating = function() {
	return data._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.parentalRating;
}

/**
 * @author Andreas Svitzer
 * Get flags
 */
var getFlags = function() {
	return data._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].system.flags;
}

/**
 * @author Andreas Svitzer
 * Get category
 */
var getCategory = function() {

	/**
	 * TODO: Get category from api
	 */
	return 'Romantik / Komedi';
}

// Export
module.exports = viaplayApi;