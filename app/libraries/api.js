// Setup obj
var api = {};
var viaplayApi = require('./viaplayApi');
var movieTrailerApi = require('./movieTrailerApi');

/**
 * @author Andreas Svitzer
 * Get data
 */
api.getByCategoryAndId = function(cat, id, masterCallback) {
	
	// Get viaplay data
	viaplayApi.getByCategoryAndId(cat, id, function(error, response, viaplayData) {

		// Get extended data from Movie Trailer API
		if (viaplayData) {
			movieTrailerApi.getByImdbId(viaplayData.imdb.id, function(trailerData) {

				// Map trailer
				viaplayData.trailer = trailerData;

				// Master callback
				masterCallback(error, response, viaplayData);	

			});		
		} else {

			// Master callback
			masterCallback(error, response, viaplayData);
		} 

	  	// Return
	  	return;
	});
}

// Export
module.exports = api;