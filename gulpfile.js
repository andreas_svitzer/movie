'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var minify = require('gulp-minify');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");

// Run sass + minify css
gulp.task('sass:compress', ['sass', 'compress-css']);

// Minify css + js
gulp.task('compress', ['compress-css', 'compress-js']);

// Compile sass to css
gulp.task('sass', function () {
  return gulp.src('./public/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/css'));
});
 
// Watch for changes in sass
gulp.task('sass:watch', function () {
  gulp.watch('./public/sass/**/*.scss', ['sass']);
});

// Minify js
gulp.task('compress-js', function() {
  gulp.src('./public/js/*.js')
    .pipe(minify({
        ext:{
            src:'.js',
            min:'-min.js'
        }
    }))
    .pipe(gulp.dest('./public/js'))
});

// Minify css
gulp.task('compress-css', function() {
  return gulp.src('./public/css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename(function (path) {
	    path.extname = "-min.css"
  	}))
    .pipe(gulp.dest('./public/css'));
});