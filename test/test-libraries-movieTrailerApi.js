/**
 * Test Model - Movie trailer api
 */ 

var assert = require('chai').assert;
var lib = require('../app/libraries/movieTrailerApi');

describe('Movie trailer api library', function() {
  	describe('#getByImdbId()', function () {
    	it('should return string', function () {
    		lib.getByImdbId('123', function(trailer){ 
    			assert.isString(trailer, 'Is a string');
    		});
    	});
  	});
});