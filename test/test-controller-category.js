/**
 * Test Controller - Category
 */ 

var request = require('supertest');
describe('loading express', function () {
	var server;
  	beforeEach(function () {
    	server = require('../server');
  	});
  	afterEach(function () {
    	//server.close();
  	});
  	it('responds to /', function testSlash(done) {
  		request(server)
    		.get('/film/the-sound-of-music-1965')
    		.expect(200, done);
  	});
  	it('404 everything else', function testPath(done) {
    	request(server)
      		.get('/foo/bar')
      		.expect(404, done);
  	});
});