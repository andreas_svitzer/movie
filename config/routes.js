'use strict';

/**
 * Module dependencies.
 */

var home = require('../app/controllers/home');
var category = require('../app/controllers/category');

/**
 * Expose
 */

module.exports = function (app, passport) {

  app.get('/', home.index, function(req, res, next) {

    /**
     * @author Andreas Svitzer
     * TODO: Cache request
     */

    next();

  });

  app.get('/:cat/:id', category.index, function(req, res, next) {

    /**
     * @author Andreas Svitzer
     * TODO: Check that url is valid
     */

    next();
    
  }, function (req, res, next) {

    /**
     * @author Andreas Svitzer
     * TODO: Cache request
     */

    next();

  });

  /**
   * Error handling
   */

  app.use(function (err, req, res, next) {
    // treat as 404
    if (err.message
      && (~err.message.indexOf('not found')
      || (~err.message.indexOf('Cast to ObjectId failed')))) {
      return next();
    }
    console.error(err.stack);
    // error page
    res.status(500).render('500', { error: err.stack });
  });

  // assume 404 since no middleware responded
  app.use(function (req, res, next) {
    res.status(404).render('404', {
      url: req.originalUrl,
      error: 'Not found'
    });
  });
};
