
/**
 * Module dependencies
 */

require('dotenv').config();

var fs = require('fs');
var join = require('path').join;
var express = require('express');
var passport = require('passport');
var config = require('./config');
var models = join(__dirname, 'app/models');
var port = 3200;
var app = express();

/**
 * Expose
 */

module.exports = app;

// Bootstrap routes
require('./config/express')(app, passport);
require('./config/routes')(app, passport);

listen();

function listen () {
  if (app.get('env') === 'test') return;
  app.listen(port);
  console.log('Express app started on port ' + port);
}